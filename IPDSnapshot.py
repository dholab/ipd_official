#!/usr/bin/python
#Script to download a current IPD snapshot that reformats and merges files
#Script removes alignment text from fasta files

import re
from subprocess import call
import os
import glob
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-download',help="Download a fresh snapshot", action='store_true')
parser.add_argument("-keeplist", help="Specify a text file with filenames to keep. Ignores .1 or .2 suffixes. One filename per line.", type=str,required=True)
args = parser.parse_args()

class ReadFasta():
	"""
	Fasta reader utility class
	"""
	
	def __init__(self,filename):
		"""
		Constructor
		"""
		self.filename = filename
		
	def openFasta(self):
		"""
		Open fasta file. Returns Dictionary object of fasta entries.
		"""
		self.fastaList = {}
		with open(self.filename) as fp:
			for name, seq in self.read_fasta(fp):
				self.fastaList[name] = seq
		return self.fastaList
		
	def read_fasta(self,fp):
		"""
		Read file line by line and separate sequences and headers
		"""
		name, seq = None, []
		for line in fp:
			line = line.rstrip()
			if line.startswith(">"):
				if name: yield (name, ''.join(seq))
				name, seq = line, []
			else:
				seq.append(line)
		if name: yield (name, ''.join(seq))
		
if args.download:
	#Download current IPD snapshot nucleotide reference files
	ftpPath = "ftp://ftp.ebi.ac.uk//pub/databases/ipd/mhc/nhp/*nuc.fasta"
	ftpPath2014 = "ftp://ftp.ebi.ac.uk//pub/databases/ipd/mhc/nhp/Updates_2014/*nuc.fasta"
	ftpPath2015 = "ftp://ftp.ebi.ac.uk//pub/databases/ipd/mhc/nhp/Updates_2015/*nuc.fasta"
	
	CWD = os.getcwd()
	os.mkdir("download")
	os.chdir("download")
	call(['wget',ftpPath])
	call(['wget',ftpPath2014])
	call(['wget',ftpPath2015])


os.mkdir("merged")
nucFilesList = glob.glob('*nuc.fasta')

nucFilesUpdateOneList = glob.glob('*nuc.fasta.1')
nucFilesUpdateOne = [name.replace('.1','') for name in nucFilesUpdateOneList]

nucFilesUpdateTwoList = glob.glob('*nuc.fasta.2')
nucFilesUpdateTwo = [name.replace('.2','') for name in nucFilesUpdateTwoList]

keepList = []
if args.download:
	with open("../" + args.keeplist,'r') as keepListFile:
		for line in keepListFile:
			fileNameKeep = line.split()[0]
			fileNameKeep = re.sub(r'\.1|\.2','',fileNameKeep)
			keepList.append(fileNameKeep)
else:
	with open(args.keeplist,'r') as keepListFile:
		for line in keepListFile:
			fileNameKeep = line.split()[0]
			fileNameKeep = re.sub(r'\.1|\.2','',fileNameKeep)
			keepList.append(fileNameKeep)

for nucFile in nucFilesList:
	reader = ReadFasta(nucFile)
	collection = reader.openFasta()
	seqCount = len(collection)
	if nucFile not in nucFilesUpdateOne and nucFile not in nucFilesUpdateTwo:
		print(nucFile," is solo")
		call(['cp',nucFile,'merged'])
	elif nucFile in nucFilesUpdateOne and nucFile not in nucFilesUpdateTwo:
		readerUpdate = ReadFasta(nucFile + '.1')
		collectionUpdate = readerUpdate.openFasta()
		seqCountUpdate = len(collectionUpdate)
		if seqCountUpdate > seqCount:
			print(nucFile," has 1 update with more sequences. Copying..")
			call(['cp',nucFile + '.1','merged/' + nucFile])
		else:
			call(['cp',nucFile,'merged'])
	else:
		readerUpdate = ReadFasta(nucFile + '.1')
		collectionUpdate = readerUpdate.openFasta()
		seqCountUpdate = len(collectionUpdate)
		
		readerUpdateTwo = ReadFasta(nucFile + '.2')
		collectionUpdateTwo = readerUpdateTwo.openFasta()
		seqCountUpdateTwo = len(collectionUpdateTwo)
		
		if seqCountUpdate > seqCount and seqCountUpdate > seqCountUpdateTwo:
			print(nucFile," has 1 update with more sequences. Copying..")
			call(['cp',nucFile + '.1','merged/' + nucFile])
		elif seqCountUpdateTwo > seqCount and seqCountUpdateTwo > seqCountUpdate:
			print(nucFile," has 2 updates with more sequences. Copying..")
			call(['cp',nucFile + '.2','merged/' + nucFile])
		else:
			call(['cp',nucFile,'merged'])
	
os.mkdir("merged/scrubbed")


mergedFiles = glob.glob('merged/*.fasta')
for mergedFile in mergedFiles:
	
	
	AMatch = re.search('Mafa-A_nuc|Mamu-A_nuc|Mane-A_nuc|Paan-A_nuc|Patr-A_nuc|Popy-A_nuc',mergedFile)
	#ManeAMatch = re.search('Mane-A',mergedFile)
	#PaanAMatch = re.search('Paan-A',mergedFile)
	#PatrAMatch = re.search('Patr-A',mergedFile)
	#PopyAMatch = re.search('Popy-A',mergedFile)
	
	#Don't process files we don't need to keep
	fileNameSplit = mergedFile.split('/')[-1]
	if fileNameSplit in keepList:
		
		reader = ReadFasta(mergedFile)
		collection = reader.openFasta()
		scrubbedFile = open('merged/scrubbed/' + fileNameSplit,'w')
		
		for allele in sorted(collection):
			#screen out HLA seqs
			HLAmatch = re.search('HLA',allele)
			AGmatch =re.search('AG',allele)
			
			if AMatch and not AGmatch or not AMatch:
				
				if not HLAmatch:
					
					#clean up name formatting for consistency
					alleleName = ""
					IPDmatch = re.search(r'(IPD:\w+\s)',allele)
					if IPDmatch:
						IPDtext = IPDmatch.group(1)
						ipdSub = re.sub(IPDtext,'',allele)
						alleleName = ipdSub + '|' + IPDtext
						#print(alleleName)
					else:
						alleleName = allele
					
					scrubbedFile.write(alleleName + '\n')
					seqText = collection[allele]
					cleanText = re.sub(r'[^ATGCatgcUMRWSYKVHDBXNumrwsykvhdbxn]','',seqText)
					
					scrubbedFile.write(cleanText + '\n')
	
Alist = ['merged/Mafa-A_nuc.fasta','merged/Mamu-A_nuc.fasta','merged/Mane-A_nuc.fasta','merged/Paan-A_nuc.fasta','merged/Patr-A_nuc.fasta','merged/Popy-A_nuc.fasta']
AGlist = ['merged/scrubbed/Mafa-AG_nuc.fasta','merged/scrubbed/Mamu-AG_nuc.fasta','merged/scrubbed/Mane-AG_nuc.fasta','merged/scrubbed/Paan-AG_nuc.fasta','merged/scrubbed/Patr-AG_nuc.fasta','merged/scrubbed/Popy-AG_nuc.fasta']


for i in range(len(Alist)):
	mafaAreader = ReadFasta(Alist[i])
	mafaAcollection = mafaAreader.openFasta()
	MafaAGFile = open(AGlist[i],'a')
	for alleleSeq in mafaAcollection:
		AGmatch =re.search('AG',alleleSeq)
		if AGmatch:
			alleleName = ""
			IPDmatch = re.search(r'(IPD:\w+\s)',alleleSeq)
			if IPDmatch:
				IPDtext = IPDmatch.group(1)
				ipdSub = re.sub(IPDtext,'',alleleSeq)
				alleleName = ipdSub + '|' + IPDtext
			else:
				alleleName = alleleSeq	
				MafaAGFile.write(alleleName + '\n')
				seqText = mafaAcollection[alleleSeq]
				cleanText = re.sub(r'[^ATGCatgcUMRWSYKVHDBXNumrwsykvhdbxn]','',seqText)
						
				MafaAGFile.write(cleanText + '\n')
#call(['mv merged/scrubbed/*.fasta ../../'],shell=True)
#call(['rm -r merged'],shell=True)
	
	